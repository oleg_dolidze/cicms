<?php
class Cms_get extends CI_Model{  

  function banner_type(){
    return array('image' => 'სურათი' , 'flash' => 'swf');
  }
  public function categories_drop(){
    $q = $this->db->get('cat')->result_array();
    $cnt = 0;
    foreach ($q as $r) {
      $t[$r['catID']] = $r['cat_name_geo'];
    }
    return $t;
  }

  public function industry(){
    $q = $this->db->from('si_industry')->order_by('sIndustry_name','asc')->get()->result_array();
    foreach ($q as $r)
      $t[$r['industryID']] = $r['sIndustry_name'];
    return $t;
  }

  public function service(){
    $q = $this->db->from('si_service')->order_by('sService_name_eng','asc')->get()->result_array();
    foreach ($q as $r)
      $t[$r['serviceID']] = $r['sService_name_eng'];
    return $t;
  }

  
  public function clients(){
    $q = $this->db->from('si_clients')->order_by('sClient_name_eng','asc')->get()->result_array();
    foreach ($q as $r)
      $t[$r['clientID']] = $r['sClient_name_eng'];
    return $t;
  }

  function set_form_vals($table,$key,$value){
    //this must be coded for next project
    /*$hidden = array('aboutID','clientID','industryID','partnerID');
    $input = array('stitle','sClient_name_eng','sIndustry_name','sPartner_name_eng');
    $text = array('txt_text','txt_eng');*/
    
    $cke = 'class="ckeditor" onload="alert(5);"';
    /*$t = json_decode($value);
      $h = br();
      $cnt = 1;
      foreach($t as $obj) {
      $h .=form_input('',$obj->key,'class=generated').' - '.
      form_input('',$obj->value,'class=generated').br();

      $cnt++;
      }
      $h .= '<div><div id="addParameter">+</div></div>';
    */


    
    
    // generate forms
    $forms[$table]=array(
			 //categoryes
			 'aboutID' => form_hidden('aboutID',$value) ,
			 'clientID' => form_hidden('clientID',$value) ,
			 'industryID' => form_hidden('industryID',$value) ,
			 'partnerID' => form_hidden('partnerID',$value) ,
			 'serviceID' => form_hidden('serviceID',$value) ,
			 'projectID'=> form_hidden('projectID',$value) ,
			 
			 'stitle' => form_input('stitle',$value) ,
			 'sClient_name_eng' => form_input('sClient_name_eng',$value) ,
			 'sService_name_eng' => form_input('sService_name_eng',$value) ,
			 'sPartner_name_eng' => form_input('sPartner_name_eng',$value) ,
			 'sIndustry_name' => form_input('sIndustry_name',$value) ,
			 'sProject_name' => form_input('sProject_name',$value) ,

			 'txt_text' => form_textarea('txt_text',$value,$cke) ,
			 'txt_eng' => form_textarea('txt_eng',$value,$cke) ,
			 'fkClients' => form_dropdown('fkClients',$this->clients(),$value),
			 'fkService' => form_dropdown('fkService',$this->service(),$value),
			 'fkService_2' => form_dropdown('fkService_2',$this->service(),$value),
			 'fkIndustry' => form_dropdown('fkIndustry',$this->industry(),$value),
			 'sort' => form_input('sort',$value)
			 //'child_id' => form_dropdown('child_id',$this->child(),$value) ,
			 );
    return $forms[$table][$key];
  }
}

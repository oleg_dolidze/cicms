<?php

class Core_m extends CI_Model {



  function get_table_config($table_name) {
    return $this->config->item($table_name, 'config');
  }
    

  function table_list_navbar() {
    $config = $this->config->item('tables', 'config');

    pre($labels);
    $dont_show = array('maillist', 'content','ci_sessions');
    $q = $this->db->query('SHOW TABLES')->result_array();
    $db_name = $this->db->database;
    foreach ($q as $r) {
      if (!in_array($r['Tables_in_' . $db_name], $dont_show)) {
	$table_name = $r['Tables_in_' . $db_name];
	$table_config = $config[$table_name];
	$icon = "<span class='".$table_config['icon']."'></span> ";
	$t[] = anchor('cms/view/' . $table_name, $icon.$table_config['label']);
      }
    }
    return $t;
  }

  function describe($table, $ret = 'Field') {
    $retArr = array();
    $query = $this->db->query("DESCRIBE `$table`")->result_array();
    foreach ($query as $row) {
      $retArr[] = $row[$ret];
    }
    return $retArr;
  }

  function get_last_id($table) {
    $q = car($this->db->query("SHOW TABLE STATUS WHERE `Name` = '$table'")->result_array());
    return $q['Auto_increment'];
  }

  function id_title($table) {
    return car($this->describe($table));
  }

  function gen_cms_table($query, $table, $showCols) {
    // query is result from database
    // table is used table name
    // show cols are columns that used to be shown
    for ($i = 0; $i < sizeof($query); $i++) {
      foreach ($showCols as $k => $v) {// k stands for key
	$rid = $query[$i][$this->core_m->id_title($table)];
	// $rid is table id name
	$t[$i][$k] = character_limiter(strip_tags($query[$i][$k]), 30);
	// strips tags from value and limits 30 chars
      }//end foreach
      $t[$i]['edit'] = anchor('cms/form/' . $table . '/' . $rid, '<span class="glyphicon glyphicon-pencil"></span>');
      // put script in link
      $t[$i]['delete'] = anchor('cms/delete/' . $table . '/' . $rid, '<span class="glyphicon glyphicon-remove-sign"></span>');
    }//end for
    return $t;
  }

  //end gen cms

  function generate_note($table, $column, $val) {
    $input = $this->config->item($table, 'input');
    $url = $input_config['url'] . $val;
    $note = $input_config['note'];
    if ($url && $note) {
      return anchor($url, $note);
    }
    return $note ? "<span>" . $note . "</span>" : \NULL;
  }

  function generate_input_forms($table, $column, $value = NULL) {
    $this->load->model('cms_input_m');
        
    $input_config = $this->get_table_config($table);
    $input_type = $input_config[$column]['type'];
    if ($attr) {
      $attr = addslashes($attr);
    }
    // future func

    if ($input_type == 'text') {
      return $this->cms_input_m->type_text($column , $value);
    } elseif ($input_type == 'hidden') {
      return form_hidden($column, $value);
    } elseif ($input_type == 'dropdown') {
      $list = $input_config[$column]['list'] ? $input_config[$column]['list'] : NULL;
      if ($list) {
	return form_dropdown($column, $list, $value);
      } else {
	$relation = $input_config[$column]['relation'];
	if ($relation) {
	  $query = $this->db->from($relation)->order_by('title_geo', 'asc')->get()->result_array();
	}
	if ($query) {
	  foreach ($query as $r) {
	    $list[$r['id']] = $r['title_geo'];
	  }
	  return form_dropdown($column, $list, $value,"class='form-control'");
	}
      } // if list is not set in configuration
    }// dropdown
    elseif ($input_type == 'date') {
      return form_input($column, $value, 'id="datepicker"');
    } elseif ($input_type == 'csv') {
      $value_arr = explode(',', $value);
      $relation = $input_config['relation'];
      if ($relation) {
	foreach ($value_arr as $val) {
	  $q = car($this->db->get_where($relation, array('id' => $val))->result_array());
	  if ($q) {
	    $tags .= '<div>' . anchor('cms/form/' . $table . '/' . $q['id'], $q['title_geo'], 'target="_blank" class="tags"') . '<span class="rm-csv" value="' . $q['id'] . '" col="' . $column . '">[X]</span></div>';
	  }
	  $items_from_relation_table = $this->db->from($relation)->order_by('title_geo', 'asc')->get()->result_array();
	  foreach ($items_from_relation_table as $item) {
	    $list[$item['id']] = $item['title_geo'];
	  }
	  $tag_dropdown = form_dropdown('', $list, '', ' id="csv-dropdown-' . $column . '" onChange="csvRelation(\'' . $table . '\',\'' . $column . '\',$(this).val())"');
	}
      }
      return form_input($column, $value, 'class="csv" id="csv-form-' . $column . '"') . br() . "<div id='csv-$column'>" . $tags . '</div> ' . $tag_dropdown;
    }// csv
    elseif ($input_type == 'textarea') {
      return form_textarea($column, $value, 'class="ckeditor"');
    } elseif ($input_type == 'bool') {
      if ($value == 1) {
	$boolval_yes = true;
	$boolval_no = false;
      }
      if ($value == 0) {
	$boolval_yes = false;
	$boolval_no = true;
      }
      return form_radio($column, 1, $boolval_yes) . " YES | " . form_radio($column, 0, $boolval_no) . " NO";
    }// bool
    elseif ($input_type == 'file') {
      return form_upload($column, NULL);
    } elseif ($input_type == 'json') {
      $cnt = 0;
      $key_val_arr = json_decode($value);
      $h = '<div  column="' . $column . '">' . form_textarea($column, $value, 'class=json-form') . br();
      foreach ($key_val_arr as $obj) {
	$relation = $input_config['relation'];
	$q = $this->db->from($relation)->order_by('title_geo', 'asc')->get()->result_array();
	$h .= '<div class="json">' . gen_dropdown(NULL, $q, 'title_geo', $obj->key, 'class="dropdown-json" column="' . $column . '"') . ' - ' . form_input('', $obj->value, 'class=input-json') . "</div>";

	$cnt++;
      }
      return $h . "<div class='add-json'><span class='glyphicon glyphicon-plus'></span></div></div>";
    }
    // json
  }

  // generate_input_forms

  function get_key_label($table, $key = NULL, $is_visible = NULL) {
    // if key is sets.gives you config of that key
    // else will give whole table key array
    $t = $this->config->item($table, 'config');
        
    if ($key) {
      return $t[$key]['label'];
    } else {
      $columns = $this->describe($table);
      foreach ($columns as $c) {
	if ($is_visible) {
	  $is_col_visible = $t[$c]['visible'];
	  if ($is_col_visible) {
	    $labels[$c] = $t[$c]['label'];
	  }
	} else {
	  $labels[$c] = $t[$c]['label'];
	}
      }// foreach
      return $labels; // return labels 
    } // else
  }

  // get_label

  function generate_filter($table_name, $key) {
    $table_conf = $this->config->item($table_name, 'config');
    $input_type = $table_conf[$key]['type'];
    $is_visible = $table_conf[$key]['visible'];
    $label = $table_conf[$key]['label'];
        
    if ($is_visible) {
      if ($input_type == 'text') {
	$label_input = lab($label) . form_input($key, '', 'class="form-control"');
      }
      if ($input_type === 'dropdown') {
	if ($table_conf[$key]['list']) {
	  //$table_conf[$key]['list'][''] = '';
	  $label_input = lab($label) . br() . form_dropdown($key, $table_conf[$key]['list'], " ","class=form-control input-group");
	}
	if ($table_conf[$key]['relation']) {
	  $vals = $this->db->from($table_conf[$key]['relation'])->order_by('title_geo', 'asc')->get()->result_array();
	  $label_input = lab($label) . br() . gen_dropdown($key, $vals, 'title_geo', NULL, "class=form-control input-group");
	}
      }
    }// is visible
    if ($label_input) {
      return '<div class="col-md-3">' . $label_input . '</div>';
    }
  }

  // generate_filter
}

// end class

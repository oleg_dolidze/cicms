<?php

$config['config']['tables'] = array(
    'categories' => array('label' => 'კატეგორიები', 'icon' => 'glyphicon glyphicon-tower'),
    'contact' => array('label' => 'კონტაქტი', 'icon' => 'glyphicon glyphicon-tower'),
    'brands' => array('label' => 'ბრენრები', 'icon' => 'glyphicon glyphicon-tower'),
    'categories' => array('label' => 'კატეგორიები', 'icon' => 'glyphicon glyphicon-tower'),
    'cities' => array('label' => 'ქალაქები', 'icon' => 'glyphicon glyphicon-tower'),
    'items' => array('label' => 'პროდუქტები', 'icon' => 'glyphicon glyphicon-tower'),
    'orders' => array('label' => 'ორდერები', 'icon' => 'glyphicon glyphicon-tower'),
    'promocodes' => array('label' => 'პრომოკოდები', 'icon' => 'glyphicon glyphicon-tower'),
    'users' => array('label' => 'მომხმარებლები', 'icon' => 'glyphicon glyphicon-tower'),

);



// documentation config.org
$config['config']['packages'] = array(
    'id' => array('type' => 'hidden', 'label' => 'id', 'visible' => true),
    'title_geo' => array('type' => 'text', 'label' => 'დასახელება ქართ', 'visible' => true),
    'title_eng' => array('type' => 'text', 'label' => 'დასახელება ინგ', 'visible' => false),
    'title_rus' => array('type' => 'text', 'label' => 'დასახელება რუს', 'visible' => false),
    'costPerMonth' => array('type' => 'text', 'label' => 'თვიური გადასახადი', 'visible' => true),
    'speed' => array('type' => 'text', 'label' => 'სიჩქარე', 'visible' => true),
    'unitLimit' => array('type' => 'text', 'label' => 'კომპიუტერესის რაოდენობა', 'visible' => true),
    'wifiLimit' => array('type' => 'text', 'label' => 'wifi ლიმიტი', 'visible' => true)
);
$config['config']['banners'] = array(
    'id' => array('type' => 'hidden', 'label' => 'id', 'visible' => true),
    'title_geo' => array('type' => 'text', 'label' => 'დასახელება', 'visible' => true),
    'url' => array('type' => 'text', 'label' => 'ბმული', 'visible' => true),
    'banner_type' => array('type' => 'dropdown', 'list' => array('image' => 'image', 'swf' => 'flash'), 'label' => 'ბანერის ტიპი', 'visible' => true)
);
$config['config']['contact'] = array(
    'id' => array('type' => 'hidden', 'label' => 'id', 'visible' => true),
    'adress_geo' => array('type' => 'text', 'label' => 'მისამართი ქართ', 'visible' => true),
    'adress_eng' => array('type' => 'text', 'label' => 'მისამართი ინგ', 'visible' => false),
    'adress_rus' => array('type' => 'text', 'label' => 'მისამართი რუს', 'visible' => false),
    'phone' => array('type' => 'text', 'label' => 'ტელეფონი', 'visible' => true),
    'mail' => array('type' => 'text', 'label' => 'მეილი', 'visible' => true),
    'location' => array('type' => 'text', 'label' => 'ადგილმდებარეობა', 'visible' => true)
);

$config['config']['gallery_packages'] = array(
    'id' => array('type' => 'hidden', 'visible' => true, 'label' => 'id'),
    'title_geo' => array('type' => 'text', 'visible' => true, 'label' => 'დასახელება ქართ'),
    'title_eng' => array('type' => 'text', 'visible' => true, 'label' => 'დასახელება ინგ'),
    'title_rus' => array('type' => 'text', 'visible' => true, 'label' => 'დასახელება რუს'),
    'body_geo' => array('type' => 'textarea', 'visible' => true, 'label' => 'არწერა ქართ'),
    'body_eng' => array('type' => 'textarea', 'visible' => true, 'label' => 'აღწერა ინგ'),
    'body_rus' => array('type' => 'textarea', 'visible' => true, 'label' => 'არწერა რუს'),
    'type' => array('type' => 'dropdown', 'list' => array('img' => 'image', 'yt' => 'youtube'), 'visible' => true, 'label' => 'ტიპი'),
    'ytURL' => array('type' => 'text', 'visible' => true, 'label' => 'youtube-ის ბმული')
);

$config['config']['gallery_shop'] = array(
    'id' => array('type' => 'hidden', 'visible' => true, 'label' => 'id'),
    'title_geo' => array('type' => 'text', 'visible' => true, 'label' => 'დასახელება ქართ'),
    'title_eng' => array('type' => 'text', 'visible' => true, 'label' => 'დასახელება ინგ'),
    'title_rus' => array('type' => 'text', 'visible' => true, 'label' => 'დასახელება რუს'),
    'body_geo' => array('type' => 'textarea', 'visible' => true, 'label' => 'არწერა ქართ'),
    'body_eng' => array('type' => 'textarea', 'visible' => true, 'label' => 'აღწერა ინგ'),
    'body_rus' => array('type' => 'textarea', 'visible' => true, 'label' => 'არწერა რუს'),
    'type' => array('type' => 'text', 'visible' => true, 'label' => 'ტიპი'),
    'ytURL' => array('type' => 'dropdown', 'list' => array('img' => 'image', 'yt' => 'youtube'), 'visible' => true, 'label' => 'youtube-ის ბმული')
);
$config['config']['orders'] = array(
    'id' => array('type' => 'hidden', 'visible' => true, 'label' => 'id'),
    'fk_user' => array('type' => 'text', 'visible' => true, 'label' => 'მომხმარებელი'),
    'fk_status' => array('type' => 'dropdown', 'relation' => 'order_status', 'visible' => true, 'label' => 'სტატუსი'),
    'order_date' => array('type' => 'date', 'visible' => true, 'label' => 'შეკვეთის თარიღი'),
    'fee' => array('type' => 'text', 'visible' => true, 'label' => 'გადასახდელი'),
    'items' => array('type' => 'csv', 'relation' => 'shop_items', 'visible' => true, 'label' => 'ნივთები')
);


$config['config']['partners'] = array(
    'id' => array('type' => 'hidden', 'visible' => true, 'label' => 'id'),
    'title_geo' => array('type' => 'text', 'visible' => true, 'label' => 'დასახელება')
);

$config['config']['shop_brands'] = array(
    'id' => array('type' => 'hidden', 'visible' => true, 'label' => 'id'),
    'title_geo' => array('type' => 'text', 'visible' => true, 'label' => 'დასახელება ქართ'),
    'title_eng' => array('type' => 'text', 'visible' => true, 'label' => 'დასახელება ინგ'),
    'title_rus' => array('type' => 'text', 'visible' => true, 'label' => 'დასახელება რუს')
);


$config['config']['shop_items'] = array(
    'id' => array('type' => 'hidden', 'visible' => true, 'label' => 'id'),
    'title_geo' => array('type' => 'text', 'visible' => true, 'label' => 'დასახელება ქართ'),
    'title_eng' => array('type' => 'text', 'visible' => true, 'label' => 'დასახელება ინგ'),
    'title_rus' => array('type' => 'text', 'visible' => true, 'label' => 'დასახელება რუს'),
    'category' => array('type' => 'text', 'visible' => true, 'label' => 'კატეგორია'),
    'brand' => array('type' => 'text', 'visible' => true, 'label' => 'ბრენდი'),
    'price' => array('type' => 'text', 'visible' => true, 'label' => 'ფასი'),
    'stock' => array('type' => 'text', 'visible' => true, 'label' => 'საწყობში'),
    'arrive_date' => array('type' => 'text', 'visible' => true, 'label' => 'ჩამოსვლის თარიღი'),
    'sale' => array('type' => 'text', 'visible' => true, 'label' => 'ფასდაკლება'),
    'new' => array('type' => 'text', 'visible' => true, 'label' => 'ახალია'),
    'body_geo' => array('type' => 'textarea', 'visible' => true, 'label' => 'არწერა ქართ'),
    'body_eng' => array('type' => 'textarea', 'visible' => true, 'label' => 'აღწერა ინგ'),
    'body_rus' => array('type' => 'textarea', 'visible' => true, 'label' => 'აღწერა რუს')
);

$config['config']['package_users'] = array(
    'id' => array('type' => 'hidden', 'visible' => true, 'label' => 'id', 'url' => base_url() . 'index.php/useractions/setuserpassword/package_users/', 'note' => 'მიანიჭეთ პაროლი'),
    'username' => array('type' => 'text', 'visible' => true, 'label' => 'მომხმარებლის სახელი'),
    'name' => array('type' => 'text', 'visible' => true, 'label' => 'სახელი'),
    'surname' => array('type' => 'text', 'visible' => true, 'label' => 'გვარი'),
    'personal_id' => array('type' => 'text', 'visible' => true, 'label' => 'პირადი ნომერი'),
    'phone' => array('type' => 'text', 'visible' => true, 'label' => 'ტელეფონი'),
    'shipping_adress' => array('type' => 'text', 'visible' => true, 'label' => 'მიღების მისამართი'),
    'email' => array('type' => 'text', 'visible' => true, 'label' => 'ელ-ფოსტა'),
    'password' => array('type' => 'text', 'visible' => false, 'label' => 'პაროლი'),
    'fk_package' => array('type' => 'dropdown', 'relation' => 'packages', 'visible' => true, 'label' => 'პაკეტი'),
    'ballance' => array('type' => 'text', 'visible' => true, 'label' => 'ბალანსი'),
    'activation_date' => array('type' => 'text', 'visible' => true, 'label' => 'გააქტიურების თარიღი'),
    'last_paused' => array('type' => 'text', 'visible' => true, 'label' => 'ბოლო დაბაუზების თარიღი'),
    'pausedDays' => array('type' => 'text', 'visible' => true, 'label' => 'დარჩენილი პაუზის დღეები'),
    'package_status' => array('type' => 'dropdown', 'list' => array('active' => 'active', 'paused' => 'paused', 'out of ballance' => 'out of ballance', 'pending' => 'pending'), 'visible' => true, 'label' => 'სტატუსი', 'filter' => 'text')
);

$config['config']['shop_users'] = array(
    'id' => array('type' => 'hidden', 'visible' => true, 'label' => 'id', 'url' => base_url() . 'index.php/useractions/setuserpassword/shop_users/', 'note' => 'მიანიჭეთ პაროლი'),
    'username' => array('type' => 'text', 'visible' => true, 'label' => 'მომხმარებლის სახელი'),
    'password' => array('type' => 'text', 'visible' => true, 'label' => 'პაროლი'),
    'name' => array('type' => 'text', 'visible' => true, 'label' => 'სახელი'),
    'surname' => array('type' => 'text', 'visible' => true, 'label' => 'გვარი'),
    'personal_id' => array('type' => 'text', 'visible' => true, 'label' => 'პირადი ნომერი'),
    'phone' => array('type' => 'text', 'visible' => true, 'label' => 'ტელეფონი'),
    'email' => array('type' => 'text', 'visible' => true, 'label' => 'მაილი'),
    'fk_status' => array('type' => 'dropdown', 'relation' => 'sale_status', 'visible' => true, 'label' => 'სტატუსი'),
    'shipping_adress' => array('type' => 'text', 'visible' => true, 'label' => 'მიტანის მისამართი')
);



$config['config']['sale_status'] = array(
    'id' => array('type' => 'hidden', 'visible' => true, 'label' => 'id'),
    'title_geo' => array('type' => 'text', 'visible' => true, 'label' => 'დასახელება ქართ'),
    'categories' => array('type' => 'json', 'relation' => 'shop_categories', 'visible' => true, 'label' => 'კატეგორიები'),
    'brands' => array('type' => 'json', 'visible' => true, 'relation' => 'shop_categories', 'label' => 'ბრენდები')
);




///// 

$config['config']['categories'] = array(
    'id' => array('type' => 'hidden', 'visible' => true, 'label' => 'id'),
    'title_geo' => array('type' => 'text', 'visible' => true, 'label' => 'დასახელება ქართ'),
    'title_eng' => array('type' => 'text', 'visible' => false, 'label' => 'დასახელება ინგ'),
    'title_rus' => array('type' => 'text', 'visible' => false, 'label' => 'დასახელება რუს'),
);

$config['config']['about'] = array(
    'id' => array('type' => 'hidden', 'visible' => true, 'label' => 'id'),
    'body_geo' => array('type' => 'text', 'visible' => true, 'label' => 'დასახელება ქართ'),
    'body_eng' => array('type' => 'text', 'visible' => true, 'label' => 'დასახელება ინგ'),
    'body_rus' => array('type' => 'text', 'visible' => true, 'label' => 'დასახელება რუს')
);


$config['config']['cities'] = array(
    'id' => array('type' => 'hidden', 'visible' => true, 'label' => 'id'),
    'title_geo' => array('type' => 'text', 'visible' => true, 'label' => 'დასახელება ქართ'),
    'title_eng' => array('type' => 'text', 'visible' => true, 'label' => 'დასახელება ინგ'),
    'title_rus' => array('type' => 'text', 'visible' => true, 'label' => 'დასახელება რუს')
);


$config['config']['brands'] = array(
    'id' => array('type' => 'hidden', 'visible' => true, 'label' => 'id'),
    'title_geo' => array('type' => 'text', 'visible' => true, 'label' => 'დასახელება ქართ'),
    'categoryID' => array('type' => 'dropdown', 'relation' => 'categories', 'visible' => true, 'label' => 'კატეგორია'),
    'title_eng' => array('type' => 'text', 'visible' => false, 'label' => 'დასახელება ინგ'),
    'title_rus' => array('type' => 'text', 'visible' => false, 'label' => 'დასახელება რუს')
);

$config['config']['contact'] = array(
    'id' => array('type' => 'hidden', 'visible' => true, 'label' => 'id'),
    'body_geo' => array('type' => 'text', 'visible' => true, 'label' => 'დასახელება ქართ'),
    'body_eng' => array('type' => 'text', 'visible' => true, 'label' => 'დასახელება ინგ'),
    'body_rus' => array('type' => 'text', 'visible' => true, 'label' => 'დასახელება რუს'),
    'mail' => array('type' => 'text', 'visible' => true, 'label' => 'მეილი'),
    'lat' => array('type' => 'text', 'visible' => true, 'label' => 'lat'),
    'lng' => array('type' => 'text', 'visible' => true, 'label' => 'lng'),
    
);


$config['config']['items'] = array(
    'id' => array('type' => 'hidden', 'visible' => true, 'label' => 'id'),
    'title_geo' => array('type' => 'text', 'visible' => true, 'label' => 'დასახელება ქართ'),
    'title_eng' => array('type' => 'text', 'visible' => true, 'label' => 'დასახელება ინგ'),
    'title_rus' => array('type' => 'text', 'visible' => true, 'label' => 'დასახელება რუს'),
    'body_geo' => array('type' => 'text', 'visible' => true, 'label' => 'აღწერა ქართ'),
    'body_eng' => array('type' => 'text', 'visible' => false, 'label' => 'აღწერა ინგ'),
    'body_rus' => array('type' => 'text', 'visible' => false, 'label' => 'აღწერა რუს'),
    'img_1' => array('type' => 'file', 'visible' => false, 'label' => 'სურათი 1'),
    'img_2' => array('type' => 'file', 'visible' => false, 'label' => 'სურათი 2'),
    'img_3' => array('type' => 'file', 'visible' => false, 'label' => 'სურათი 3'),
    'categoryID' => array('type' => 'dropdown', 'relation' => 'categories', 'visible' => true, 'label' => 'კატეგორია'),
    'brandID' => array('type' => 'dropdown', 'relation' => 'brands', 'visible' => true, 'label' => 'ბრენდი'),
    'saleBool' => array('type' => 'bool',  'visible' => true, 'label' => 'ფასდაკლება'),
    'SaleNewPrice' => array('type' => 'text', 'visible' => true, 'label' => 'ფასდაკლების ფასი'),
    'promoCodeAffects' => array('type' => 'bool',  'visible' => true, 'label' => 'პრომო კოდის მოქმედება'),
    'visibleRating' => array('type' => 'bool',  'visible' => false, 'label' => 'რეიტინგის გამოჩენა'),
    'totalVotes' => array('type' => 'text', 'visible' => false, 'label' => 'ხმების ქულები'),
    'totalHits' => array('type' => 'text', 'visible' => false, 'label' => 'ხმის მიმხემელთა რაოდენობა'),
    'shippingInTbilisi' => array('type' => 'text', 'visible' => false, 'label' => 'თბილისში შიპინგი'),
    'shippingInRegion' => array('type' => 'text', 'visible' => false, 'label' => 'რეგიონში შიპინგი'),
    'fastShippingInTbilisi' => array('type' => 'text', 'visible' => false, 'label' => 'სწრაფი თბილისში შიპინგი'),
    'fastShippingInRegion' => array('type' => 'text', 'visible' => false, 'label' => 'სწრაფი რეგიონში შიპინგი'),
    

);


$config['config']['orders'] = array(
    'id' => array('type' => 'hidden', 'label' => 'id', 'visible' => true),
    'trackingID' => array('type' => 'text', 'label' => 'თრექინგის კოდი', 'visible' => true),
    'dateTime' => array('type' => 'date', 'label' => 'ჩამოსვლის თარიღი', 'visible' => true),
    'status' => array('type' => 'dropdown', 'list' => array('one' => 'one', 'two' => 'two'), 'label' => 'სტატუსი', 'visible' => true)
);


$config['config']['promocodes'] = array(
    'id' => array('type' => 'hidden', 'visible' => true, 'label' => 'id'),
    'title_geo' => array('type' => 'text', 'visible' => true, 'label' => 'დასახელება ქართ'),
    'title_eng' => array('type' => 'text', 'visible' => true, 'label' => 'დასახელება ინგ'),
    'title_rus' => array('type' => 'text', 'visible' => true, 'label' => 'დასახელება რუს'),
    'code' => array('type' => 'text', 'visible' => true, 'label' => 'კოდი'),
    'dueData' => array('type' => 'date', 'label' => 'მოქმედების ვადა', 'visible' => true),
    'categoryList' => array('type' => 'json', 'relation' => 'categories', 'visible' => true, 'label' => 'კატეგორიები'),
    'brandList' => array('type' => 'json', 'visible' => true, 'relation' => 'brands', 'label' => 'ბრენდები'),
    'itemList' => array('type' => 'json', 'relation' => 'items', 'visible' => true, 'label' => 'კატეგორიები'),
    'codeUsability' => array('type' => 'bool',  'visible' => true, 'label' => 'კოდის გამოყენებადობა'),
    'codeStatus' => array('type' => 'bool',  'visible' => true, 'label' => 'კოდის სტატუსი'),
);
